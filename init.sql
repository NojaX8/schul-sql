-- Datenbanken: Projektarbeit (Noel Hödtke & Kolja Danowski)
-- Kundendatenbank inklusive Verbindung zu Bestell- und Zahlungssystem

-- DATABASE CREATION

DROP DATABASE IF EXISTS kverw;

CREATE DATABASE kverw;

USE kverw;

CREATE TABLE kontaktperson (
  id INT(64) NOT NULL  AUTO_INCREMENT,
  name VARCHAR(255),
  nachname VARCHAR(255),
  vorname VARCHAR(255),
  PRIMARY KEY(id)
);

CREATE TABLE kontakt (
  fk_kontaktperson INT(64),
  kontaktdatum VARCHAR(255),
  typ VARCHAR(16),
  FOREIGN KEY(fk_kontaktperson) REFERENCES kontaktperson(id)
);

CREATE TABLE kunde (
  id INT(64) NOT NULL AUTO_INCREMENT,
  fk_kontaktperson INT(64),
  name VARCHAR(255),
  adresse VARCHAR(255),
  seit DATE,
  PRIMARY KEY(id),
  FOREIGN KEY(fk_kontaktperson) REFERENCES kontaktperson(id)
);

CREATE TABLE bankdaten (
  fk_kunde INT(64),
  iban VARCHAR(32),
  bic CHAR(8),
  use_as_primary BOOLEAN,
  FOREIGN KEY(fk_kunde) REFERENCES kunde(id)
);

CREATE TABLE bestellung (
  id INT(64) NOT NULL AUTO_INCREMENT,
  fk_kunde INT(64),
  bestellt_am DATE,
  summe INT(64),
  bezahlt INT(64),
  waehrung CHAR(3),
  letzte_zahlung DATETIME,
  PRIMARY KEY(id),
  FOREIGN KEY(fk_kunde) REFERENCES kunde(id)
);

CREATE TABLE zahlung (
  id INT(64) NOT NULL AUTO_INCREMENT,
  fk_bestellung INT(64),
  datum DATETIME,
  iban VARCHAR(32),
  bic CHAR(8),
  wert INT(64),
  PRIMARY KEY(id),
  FOREIGN KEY(fk_bestellung) REFERENCES bestellung(id)
);

-- END DATABASE CREATION

-- START PROCEDURES

DROP FUNCTION IF EXISTS is_iban;
DROP TRIGGER IF EXISTS zahlung_validate_iban;
DROP TRIGGER IF EXISTS bankdaten_validate_iban;
DROP PROCEDURE IF EXISTS create_zahlung;

DELIMITER //
CREATE FUNCTION is_iban (i_iban VARCHAR(32)) RETURNS BOOLEAN
BEGIN
  DECLARE v_iban_code INT(30) DEFAULT NULL;
  DECLARE EXIT HANDLER FOR sqlexception
    BEGIN
      RETURN FALSE;
    END;
  IF i_iban IS NOT NULL THEN
    SELECT cast(substr(i_iban, 3) AS UNSIGNED) FROM dual INTO v_iban_code;
    IF mod(v_iban_code, 97) = 1 THEN
      RETURN TRUE;
    END IF;
  END IF;
  RETURN FALSE;
END//


CREATE TRIGGER zahlung_validate_iban
  BEFORE INSERT ON zahlung
  FOR EACH ROW
BEGIN
  DECLARE v_is_iban BOOLEAN DEFAULT FALSE;
  SET v_is_iban = is_iban(new.iban);
  IF NOT v_is_iban = TRUE THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Field "iban" does not contain a valid IBAN';
  END IF;
END//

CREATE TRIGGER bankdaten_validate_iban
  BEFORE INSERT ON bankdaten
  FOR EACH ROW
BEGIN
  DECLARE v_is_iban BOOLEAN DEFAULT FALSE;
  SET v_is_iban = is_iban(new.iban);
  IF NOT v_is_iban = TRUE THEN
    SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = 'Field "iban" does not contain a valid IBAN';
  END IF;
END//

CREATE PROCEDURE create_zahlung_simple (
  IN iv_bestellung INT(64),
  IN iv_kunde INT(64),
  IN iv_wert INT(64)
)
BEGIN
  DECLARE v_iban VARCHAR(32);
  DECLARE v_bic CHAR(8);
  DECLARE v_exists INT DEFAULT 0;
  DECLARE v_is_iban BOOLEAN DEFAULT FALSE;
  SELECT count(id) FROM bestellung
    WHERE fk_kunde = iv_kunde
    AND id = iv_bestellung;
  SET v_is_iban = is_iban(v_iban);
  INTO v_exists;
  IF v_exists=0 THEN
    SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = 'Combination of Bestellungs-ID and Kunde does not exist';
    LEAVE;
  END IF;
  SELECT iban, bic FROM bankdaten
    INTO v_iban, v_bic
    WHERE fk_kunde = iv_kunde
    AND use_as_primary = TRUE;
  INSERT INTO zahlung(fk_bestellung, datum, iban, bic, wert)
    VALUES(iv_bestellung, now(), v_iban, v_bic, iv_wert);
END//
DELIMITER ;

